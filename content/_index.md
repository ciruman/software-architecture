## Welcome

This website was generated to simplify the access to the presentations related to the architecture presentations.

Currently, the following are available:
* [Software architecture fundamentals](https://ciruman.gitlab.io/software-architecture/post/fundamentals/)
* [Software architecture types](https://ciruman.gitlab.io/software-architecture/post/architecture_types/)
* [Software architecture dojo](https://ciruman.gitlab.io/software-architecture/post/dojo/)


This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io).
