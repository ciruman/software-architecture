---
title: Fundamentals of Software architecture 
subtitle: Fundamententals presentation
date: 2021-07-19
tags: ["software_architecture", "fundamentals"]
---

This presentation was created to explain the basics about software architecture and specially related to agile methodologies.

[Access the software architecture fundamentals presentation](https://ciruman.gitlab.io/software-architecture-presentations/architecture-fundamentals.html)


